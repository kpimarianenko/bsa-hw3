import * as config from "./config";
import { getRandomInt } from "../helpers/random";
import { texts } from "../data";

let rooms = [];
let users = [];

const gameMap = new Map();
const getCurrentRoomName = socket => Object.keys(socket.rooms).find(roomName => gameMap.has(roomName));

export default io => {
  io.on("connection", socket => {
    const user = JSON.parse(socket.handshake.query.user);

    if (users.filter(item => item.name === user.name).length) {
      socket.emit("LOGIN_ERROR", "User with this username is already loged in");
      return;
    }

    users.push(user);
    socket.emit("UPDATE_ROOMS", getRoomsInfo(rooms));
    console.log(`${user.name} connected`);

    socket.on("disconnect",() => {
      const data = getUsersRoom(user.name);
      disconnectUser(data.room, user);
      if (data.room) {
        deleteRoomIfEmpty(io, data.name);
        io.to(data.name).emit("UPDATE_USERS_IN_ROOM", data.room.users);
        startGame(io, data.room.users, data.name);
        io.emit("UPDATE_ROOMS", getRoomsInfo(rooms));
        if (data.room.users.length === data.room.result.length) {
          endGame(io, data.room, data.name, data.room.result);
        }
      }
      console.log(`${user.name} disconnected`);
    });

    socket.on("CREATE_ROOM", (rawName) => {
      const name = rawName.trim();
      if (name.length <= 0 || rooms.includes(name)) {
        socket.emit("WRONG_ROOM_NAME", "Room name is invalid");
        return;
      }
      rooms.push(name);
      gameMap.set(name, {
        users: [],
        result: [],
        isGameStarted: false,
        timerID: -1
      });
      io.emit("UPDATE_ROOMS", getRoomsInfo(rooms));
      socket.emit("ROOM_CREATION_DONE", name);
    });

    socket.on("JOIN_ROOM", (name) => {
      socket.join(name, () => {
        const room = gameMap.get(name);
        room.users.push(user);
        gameMap.set(name, room);
        socket.emit("JOIN_ROOM_DONE", name);
        io.emit("UPDATE_ROOMS", getRoomsInfo(rooms));
        io.to(name).emit("UPDATE_USERS_IN_ROOM", room.users);
      });
    });

    socket.on("LEAVE_ROOM", () => {
      const roomName = getCurrentRoomName(socket);
      socket.leave(roomName, () => {
        const room = gameMap.get(roomName);
        room.users = room.users.filter(value => value.name !== user.name);
        room.result = room.result.filter(value => value.name !== user.name);
        deleteRoomIfEmpty(io, roomName);
        io.emit("UPDATE_ROOMS", getRoomsInfo(rooms));
      });
    });
    
    socket.on("TOGGLE_READY", (item) => {
      const roomName = getCurrentRoomName(socket);
      const { name, isReady } = item;
      users = users.map(user => {
        if (user.name === name) {
          user.isReady = isReady;
        }
        return user;
      });
      const roomUsers = gameMap.get(roomName).users;
      io.to(roomName).emit("UPDATE_USERS_IN_ROOM", roomUsers);
      startGame(io, roomUsers, roomName);
    });

    socket.on("KEY_DOWN", (user, length) => {
      const roomName = getCurrentRoomName(socket);
      const room = gameMap.get(roomName);
      room.users = room.users.map(item => {
        if (item.name === user.name) {
          item.progress = user.progress;
        }
        return item;
      });
      io.to(roomName).emit("UPDATE_USERS_IN_ROOM", room.users);
      if (user.progress / length === 1) {
        room.result.push(user);
      }
      gameMap.set(roomName, room);
      if (room.users.length === room.result.length) {
        endGame(io, room, roomName, room.result);
      }
    });
  });
};

const endGame = (io, room, roomName, result) => {
  io.to(roomName).emit("END_GAME", result);
  room.result.length = 0;
  room.isGameStarted = false;
  clearInterval(room.timerID);
  room.timerID = -1;

  room.users = room.users.map(user => {
    user.progress = 0;
    return user;
  });
  gameMap.set(roomName, room);
  io.emit("UPDATE_ROOMS", getRoomsInfo(rooms));
}

const startGame = (io, users, roomName) => {
  if (users.length >= 2 && users.every(user => user.isReady)) {
    const textIndex = getRandomInt(0, texts.length - 1);
    let cooldown = config.SECONDS_TIMER_BEFORE_START_GAME;
    io.to(roomName).emit("START_CD_TIMER", cooldown--, textIndex);
    const room = gameMap.get(roomName);
    room.isGameStarted = true;
    gameMap.set(roomName, room);
    io.emit("UPDATE_ROOMS", getRoomsInfo(rooms));
    const intervalCode = setInterval(() => {
      io.to(roomName).emit("UPDATE_CD_TIMER", cooldown);
      if (cooldown < 0) {
        clearInterval(intervalCode);
        io.to(roomName).emit("START_GAME");
        startIngameTimer(io, room, roomName)
      }
      cooldown--;
    }, 1000);
  }
}

const startIngameTimer = (io, room, roomName) => {
  let time = config.SECONDS_FOR_GAME;
  io.to(roomName).emit("START_INGAME_TIMER", time--);
  room.timerID = setInterval(() => {
    io.to(roomName).emit("UPDATE_INGAME_TIMER", time);
    if (time < 0) {
      const otherPlayers = room.users.filter(user => {
        return !room.result.filter(item => item.name === user.name).length;
      })
      otherPlayers.sort((a, b) => a.progress < b.progress ? 1 : -1);
      endGame(io, room, roomName, [...room.result, ...otherPlayers]);
    }
    time--;
  }, 1000);
}

const disconnectUser = (room, user) => {
  const filterCb = value => value.name !== user.name;
  users = users.filter(filterCb);
  if (room) {
    room.users = room.users.filter(filterCb);
    room.result = room.result.filter(filterCb);
  }
}

const getRoomsInfo = (rooms) => {
  const roomsInfo = [];
  rooms.forEach(roomName => {
    const room = gameMap.get(roomName);
    if (!room.isGameStarted && room.users.length < config.MAXIMUM_USERS_FOR_ONE_ROOM) {
      roomsInfo.push({
        name: roomName,
        connections: room.users.length
      });
    }
  });
  return roomsInfo;
}

const getUsersRoom = (username) => {
  let usersRoomInfo = {};
  gameMap.forEach((room, name) => {
    if(room.users.filter(user => user.name === username).length)
      usersRoomInfo = {
        room: gameMap.get(name),
        name
      }
  });
  return usersRoomInfo;
}

const deleteRoomIfEmpty = (io, roomName) => {
  const room = gameMap.get(roomName);
  if (room.users.length > 0) {
    gameMap.set(roomName, room);
    io.to(roomName).emit("UPDATE_USERS_IN_ROOM", room.users);
  } else {
    rooms = rooms.filter(room => room !== roomName);
    clearInterval(room.timerID);
    gameMap.delete(roomName);
  }
}