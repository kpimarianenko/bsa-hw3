import { createElement } from './helpers/domHelper.mjs';
import { createUser, createModalBodyFromRating, showError } from './gameView.mjs';
import { getTextById } from './helpers/apiHelper.mjs';
import { showModal } from './modal.mjs';

const game = document.getElementById("game");
const ready = document.getElementById("ready-button");
const back = document.getElementById("back-button");
const timer = document.getElementById("timer");
const cdTimer = document.getElementById("cd-timer");
const list = document.getElementById("user-list");
const roomsPage = document.getElementById("rooms-page");
const gamePage = document.getElementById("game-page");
const username = sessionStorage.getItem("username");

let text = "";

const startCdTimer = (value, id) => {
  hideButtons();
  updateCdTimer(value);
  getTextById(id)
    .then(t => text = t.text)
    .catch(e => console.error(e.message));
}

const updateCdTimer = (value) => {
  cdTimer.innerText = value;
}

const updateGameTimer = (value) => {
  timer.innerText = `${value} seconds left`
}

const hideButtons = () => {
  ready.classList.add("display-none");
  back.classList.add("display-none");
}

const showButtons = () => {
  ready.classList.remove("display-none");
  back.classList.remove("display-none");
}

const user = {
  name: username,
  isReady: false,
  progress: 0
};

if (!username) {
  window.location.replace("/login");
}

export const socket = io("", { query: { user: JSON.stringify(user) } });

const onReady = (event) => {
  user.isReady = !user.isReady;
  if (user.isReady) {
    ready.innerText = "Not ready";
    ready.className = "not-ready";
  } else {
    ready.innerText = "Ready";
    ready.className = "ready";
  }
  socket.emit("TOGGLE_READY", user);
}

const onBack = (event) => {
  roomsPage.classList.remove("display-none");
  gamePage.classList.add("display-none");
  socket.emit("LEAVE_ROOM");
}

const onKeyDown = (event) => {
  if (user.progress < text.length && event.key === text.substring(user.progress, user.progress + 1)) {
    user.progress++;
    updateTypedText(user.progress);
    socket.emit("KEY_DOWN", user, text.length);
  }
}

const start = () => {
  cdTimer.innerText = "";
  updateTypedText(0);
  console.log("added")
  document.addEventListener("keydown", onKeyDown);
}

const end = (rating) => {
  onReady();
  showButtons();
  timer.innerText = "";
  user.progress = 0;
  document.removeEventListener("keydown", onKeyDown);

  showModal({
    title: "Result:",
    bodyElement: createModalBodyFromRating(rating, text)
  });

  text = "";
  updateTypedText(0);
  socket.emit("RESET_PROGRESS");
}

const updateTypedText = (progress) => {
  const textToTypeBlock = createElement({ tagName: "span", className: "text-to-type" });
  const coloredTextBlock = createElement({ tagName: "span", className: "text-typed" });
  const currentLetterBlock = createElement({ tagName: "span", className: "next-letter" });
  const notColoredTextBlock = createElement({ tagName: "span", className: "text-not-typed" });
  coloredTextBlock.innerText = text.slice(0, progress);
  currentLetterBlock.innerText = text.slice(progress, progress + 1);
  notColoredTextBlock.innerText = text.slice(progress + 1);
  textToTypeBlock.appendChild(coloredTextBlock);
  textToTypeBlock.appendChild(currentLetterBlock);
  textToTypeBlock.appendChild(notColoredTextBlock);
  game.removeChild(game.lastChild);
  game.appendChild(textToTypeBlock);
}

const updateRoom = (users) => {
  list.innerHTML = "";
  users.forEach(user => {
    list.appendChild(createUser(user, username, text))
  });
}

ready.onclick = onReady;
back.onclick = onBack;
socket.on("UPDATE_USERS_IN_ROOM", updateRoom);
socket.on("LOGIN_ERROR", (error) => {
  sessionStorage.removeItem("username");
  showError(error, () => window.location.replace("/login"));
});
socket.on("START_CD_TIMER", startCdTimer);
socket.on("UPDATE_CD_TIMER", updateCdTimer);
socket.on("START_INGAME_TIMER", updateGameTimer);
socket.on("UPDATE_INGAME_TIMER", updateGameTimer);
socket.on("START_GAME", start);
socket.on("END_GAME", end);