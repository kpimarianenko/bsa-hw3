import { createElement } from './helpers/domHelper.mjs';
import { showModal } from './modal.mjs';

export const createUser = ({ name, isReady, progress }, username, text) => {
  const progressPercentage = progress / text.length * 100;

  const userBl = createElement({ tagName: "div", className: "user" });
  const userInfoBl = createElement({ tagName: "div", className: "flex user-info" });
  const userStatusBl = createElement({
    tagName: "div",
    className: `user-status status-${isReady ? "ready" : "not-ready"}`
  });
  const userNameBl = createElement({ tagName: "p", className: "user-name" });
  const userProgBarBl = createElement({ tagName: "div", className: "user-progress-bar bordered" });
  const userProgBl = createElement({
    tagName: "div",
    className: `user-progress ${progressPercentage >= 100 ? "done" : ""}`
  });

  userProgBl.style.width = `${progressPercentage}%`;
  userNameBl.innerText = name;

  userInfoBl.appendChild(userStatusBl);
  userInfoBl.appendChild(userNameBl);
  userProgBarBl.appendChild(userProgBl);
  userBl.appendChild(userInfoBl);
  userBl.appendChild(userProgBarBl);

  if (username === name) {
    const userYouBl = createElement({ tagName: "p", className: "user-you" });
    userYouBl.innerText = "(You)";
    userInfoBl.appendChild(userYouBl);
  }

  return userBl;
}

export const createModalBodyFromRating = (rating, text) => {
  const bl = createElement({ tagName: "div", className: "modal-body" })
  rating.forEach((user, index) => {
    const userBl = createElement({ tagName: "p", className: "user-rating" })
    userBl.innerText = `${index + 1}. ${user.name} - ${(user.progress / text.length * 100).toFixed(2)}%`;
    bl.appendChild(userBl);
  })
  return bl;
}

export const createModalForm = (id, onClick = () => {}, btnText = "Submit") => {
    const modalBody = createElement({ tagName: "div", className: "modal-body form" });
    const input = createElement({ tagName: "input", attributes: { id }});
    const submitBtn = createElement({ tagName: "button" });

    submitBtn.onclick = onClick;
    submitBtn.innerText = btnText;
    modalBody.appendChild(input);
    modalBody.appendChild(submitBtn);

    return modalBody;
}

export const showError = (msg, onClose) => {
  const message = createElement({ tagName: "div", className: "modal-body" });
  message.innerText = msg;
  showModal({ title: "Error", bodyElement: message, onClose });
}

export const createRoomBlock = (room, onJoin) => {
    const roomBl = createElement({ tagName: "div", className: "room bordered" });
    const connectedBl = createElement({ tagName: "div", className: "room-connected" });
    const nameBl = createElement({ tagName: "div", className: "room-name" });
    const joinBtn = createElement({ tagName: "button", className: "room-join" });

    connectedBl.innerText = `${room.connections} users connected`;
    nameBl.innerText = room.name;
    joinBtn.innerText = "Join";
    joinBtn.onclick = onJoin;

    roomBl.appendChild(connectedBl);
    roomBl.appendChild(nameBl);
    roomBl.appendChild(joinBtn);

    return roomBl;
}