import { showModal, hideModal } from './modal.mjs';
import { createModalForm, createRoomBlock } from './gameView.mjs';
import { socket } from './game.mjs'; 
import { createElement } from './helpers/domHelper.mjs';
import { showError } from './gameView.mjs';

const roomName = document.getElementById("room-name");
const roomsPage = document.getElementById("rooms-page");
const gamePage = document.getElementById("game-page");
const createRoom = document.getElementById("create-room");
const rooms = document.getElementById("room-list");

const showModalWindow = () => {
  const inputId = "room-name-input";
  showModal({
    title: "Enter room name",
    bodyElement: createModalForm(inputId, () => {
      const roomName = document.getElementById(inputId).value;
      socket.emit("CREATE_ROOM", roomName);
      hideModal();
    })
  })
}

const updateRooms = (items) => {
  rooms.innerHTML = '';
  items.forEach(room => {
    const roomBl = createRoomBlock(room, () => {
        socket.emit("JOIN_ROOM", room.name);
    });
    rooms.appendChild(roomBl);
  });
}

const onRoomCreated = (name) => {
    socket.emit("JOIN_ROOM", name);
}

const joinRoom = (name) => {
  roomsPage.classList.add("display-none");
  gamePage.classList.remove("display-none");
  roomName.innerText = name;
}

createRoom.onclick = showModalWindow;
socket.on("WRONG_ROOM_NAME", showError)
socket.on("UPDATE_ROOMS", updateRooms)
socket.on("ROOM_CREATION_DONE", onRoomCreated)
socket.on("JOIN_ROOM_DONE", joinRoom)